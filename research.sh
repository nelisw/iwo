#!/bin/bash
#Calculate amount of deadly accidents on high speed roads
tail -n +2 ongevallen-20**.csv | cut -d',' -f13,25 | grep -e ",130" -e ",120" -e ",110" -e ",100" -e ",90" | grep -v "^0," | wc -l
#Calculate amount of non deadly accidents on high speed roads
tail -n +2 ongevallen-20**.csv | cut -d',' -f13,25 | grep -e ",130" -e ",120" -e ",110" -e ",100" -e ",90" | grep "^0," | wc -l
#Calculate amount of deadly accidents of low speed roads
tail -n +2 ongevallen-20**.csv | cut -d',' -f13,25 | grep -e ",30" -e ",50" -e ",60" -e ",70" -e ", 80" | grep -v "^0," | wc -l
#Calculate amount of non deadly accidents on low speed roads
tail -n +2 ongevallen-20**.csv | cut -d',' -f13,25 | grep -e ",30" -e ",50" -e ",60" -e ",70" -e ", 80" | grep "^0," | wc -l
